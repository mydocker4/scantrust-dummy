const express = require("express");
const fs = require("fs");
const properties = require("../package.json");

const scmRoute = express.Router();

scmRoute.get("/about", (req, res) => {
    const aboutInfo = {
        name: properties.name,
        description: properties.description,
        author: properties.author
    }
    res.json(aboutInfo)
})

scmRoute.get("/logs", (req, res) => {
    const file = `./log.log`;
    res.setHeader("content-type", "text/plain");
    fs.createReadStream(file).pipe(res);
})

scmRoute.post("/upload/async/", (req, res) => {
    const isSuccess = randomResult(2);
    const resData = {
        "task_id": "?success=2"
    }
    res
        .status(isSuccess ? 200 : 500)
        .json(resData)
})

scmRoute.post("/upload", (req, res) => {
    // const resData = {
    //     "task_id": "?success=2"
    // };

    // const errorData = {
    //     "errors": [
    //         {
    //             "message": "Invalid value passed for extended_id: '2AFEDB9A420410MRP041044F47CE86E'",
    //             "line": "1",
    //             "field": "extended_id",
    //             "key_value": "2AFEDB9A420410MRP041044F47CE86E"
    //         }
    //     ]
    // };
    // const params = { ...req.params };
    // const isNotValid = params["status_code"] == "400" ? true : false;

    // res
    //     .status(isNotValid ? 400 : 200)
    //     .json(isNotValid ? errorData : resData)


    const errorData = {
        "errors": [
            {
                "message": "Internal server"
            }
        ]
    };
    res
        .status(200)
        .json(errorData)
})


scmRoute.get("/tasks/", (req, res) => {
    const isSuccess = randomResult(parseInt(req.query?.success));
    const resData = {
        state: isSuccess ? "pending" : "complete"
    }
    res
        .status(200)
        .json(resData)
})

function randomResult(success) {
    if (success === 0) return false;
    if (success === 1) return true;

    let min = 0;
    let rand = Math.floor(Math.random() * (success - min) + min);
    return rand === 1;
}

module.exports = scmRoute
