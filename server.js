const bodyParser = require("body-parser");
const fs = require("fs");
const morgan = require("morgan");
const express = require("express");
const scmRouter = require("./routes/scm");

const PORT = 8980;
const HOST_NAME = "0.0.0.0";

const app = express();

const originalSend = app.response.send
app.response.send = function sendOverWrite(body) {
  originalSend.call(this, body)
  this.__custombody__ = body
}
morgan.token('res-body', (_req, res) =>
  JSON.stringify(res.__custombody__),
)
// morgan format with custom token
const morganFormat = ':method :url :status :response-time ms - :res[content-length] :res-body';
// use morgan log with format above
const logFile = fs.createWriteStream('./log.log', {flags: 'a'}); //use {flags: 'w'} to open in write mode
app.use(morgan(morganFormat, { stream: logFile }));

// use morgan log with default
//app.use(morgan('combined', { stream: logFile }));

app.use(express.static("client"));

app.use(bodyParser.urlencoded({extended: true}));

app.use("/api/v2/scm", scmRouter);

app.listen(PORT, HOST_NAME, () => {
    console.log(`Server running at ${HOST_NAME}:${PORT}`)
})
